tools-namespace-clojure (1.3.0-3) unstable; urgency=medium

  * Team upload.
  * add missing classpaths (Closes: #1076115)
  * fix ftbfs due to missing debian version for clojure
  * d/control: fix Vcs-* URLs
  * d/control: bump Standards-Version, no changes needed
  * d/tests: drop runtime package deps from control
  * run wrap-and-sort -bastk

 -- Jérôme Charaoui <jerome@riseup.net>  Wed, 10 Jul 2024 20:36:44 -0400

tools-namespace-clojure (1.3.0-2) unstable; urgency=medium

  * Team upload.
  * modernise build. (Closes: #1073107)
  * d/tests: add autopkgtests.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Wed, 03 Jul 2024 13:32:36 -0400

tools-namespace-clojure (1.3.0-1) experimental; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * d/control: Migrate to the Clojure Team.

  [ Rob Browning ]
  * Add initial debian/gbp.conf.
  * Update debian/watch to use mode=git and refs/tags.
  * Update to upstream version 1.3.0.
  * Choose clojure N.x (e.g. 1.x) in debian/maven.rules.
  * debian/control: wrap-and-sort.
  * debian/control: build-depend on default-jdk-headless, not default-jdk.
  * Update debian/gbp.conf for master -> debian/main.

 -- Rob Browning <rlb@defaultvalue.org>  Mon, 26 Dec 2022 14:54:22 -0600

tools-namespace-clojure (0.2.11-1) unstable; urgency=medium

  * Initial release (closes: #855769)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Thu, 03 Aug 2017 12:09:20 -0400
